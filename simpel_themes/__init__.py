from simpel_themes.version import get_version

VERSION = (0, 1, 5, "final", 0)

__version__ = get_version(VERSION)
